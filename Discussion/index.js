// [SECTION] While Loop

/*
	Syntax
	while (expression/condition) {
		statement
	}
*/

// While the value of count is not equal to 0

let count = 5;

while(count !==0){
	console.log("While: " + count);

	// iteration - it increases the value of count after every iteration to stop the loop when it reaches 5

	// ++ increment -- decrement
	count--;
}

// [SECTION] Do While Loop

// A do-while loop works a lot like the while loop.But unlike while loops, do-while loops guarantee that the code will be executed at least once.

/*
	Syntax:

	do {
		statement
	} while (expression/condition)
*/


// let number = Number(prompt("Give me a number"));

// do {

// 	console.log("Do While: " + number);

// 	number += 1;

// } while (number < 10)

// [SECTION] For Loops

// A for loop is more flexible than while and do-while loops.

/*
	Syntax
	for (initialization; expression/condition; finalExpression) {
		statement
	}
*/

// for (let count = 0; count <= 20; count ++){
// 	console.log(count); 
// }

// // let myString = "Alex";
// // Characters in strings may be counted using the .length property.

// // console.log(myString.length);

// // console.log(myString[0]);
// // console.log(myString[1]);
// // console.log(myString[2]);
// // console.log(myString[3]);

// let myString = prompt("give me a word");

// for (let x = 0; x < myString.length; x++){
// 	console.log(myString[x])
// }

// // Changing of vowels using loops

// let myName = "DelIZo";

// for (let i = 0; i < myName.length; i++){
// 	if (
// 		myName[i].toLowerCase() == "a" ||
// 		myName[i].toLowerCase() == "e" ||
// 		myName[i].toLowerCase() == "i" ||
// 		myName[i].toLowerCase() == "o" ||
// 		myName[i].toLowerCase() == "u"
// 		){
// 		console.log(3);
// 	}	else {
// 		console.log(myName[i]);
// 	}
// }